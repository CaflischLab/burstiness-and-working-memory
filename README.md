Scripts supporting the following paper:

# Spiking burstiness and working memory in the human medial temporal lobe

**Francesco Cocina**, **Andreas Vitalis**\*, **Amedeo Caflisch**\*

*Cerebral Cortex Communications*, 3 (4), tgac039, 2022. DOI:[10.1093/texcom/tgac039](https://doi.org/10.1093/texcom/tgac039)

### Abstract:
*Persistent activity has commonly been considered to be a hallmark of working memory (WM). Recent evidence indicates that neuronal discharges in the medial temporal lobe (MTL) are compatible with WM neural patterns observed in cortical areas. However, the characterization of this activity rarely consists of measurements other than firing rates of single neurons. Moreover, a varied repertoire of firing dynamics has been reported in the MTL regions, which motivate the more detailed examination of the relationships between WM processes and discharge patterns undertaken here. Specifically, we investigate‚ at different resolution levels, firing irregularities in electrode recordings from the hippocampus, amygdala, and the entorhinal cortex of epileptic patients during a WM task. We show that some types of (ir)regularities predict response times of the patients depending on the trial periods under consideration. Prominent burst activity at the population level is observed in the amygdala and entorhinal cortex during memory retrieval. In general, regular and bursty neurons contribute to the decoding of the memory load, yet they display important differences across the three anatomical areas. Our results suggest that nonrandom (non-Poisson) patterns are relevant for WM, which calls for the development and use of statistics complementary to mere spike counts.**

&nbsp;  

***
***

&nbsp;  
Below a description of the script files in `R/` directory
&nbsp;  

#### H5 Database extraction and data cleaning

`h5_extract_data.R` &rarr; Data extraction from the .h5 files.  
`h5_check_and_process.R` &rarr; Checking and processing the data extracted above. Used also to reproduce some numbers of the original paper.  
`cross_talk.R` &rarr; Identification of artificial cross-talk between units.


#### General burstiness analysis. Figures 1, 2, S1

`change_points.R` &rarr; Identifying change points. Production file.  
`windowed_lvr_frate.R` &rarr; Windowed calculation of LvR and firing rates. Production file.  
`experiment_description.R` &rarr; Illustrative descriptions of Figs. 1a-b.  


`burstiness_figure_1.R` &rarr; Analysis and plot of Fig. 1.  
`burstiness_figure_2.R` &rarr; Analysis and plot of Fig. 2. Results of `combos_lvr.R` are needed.  
`burstiness_figure_S1.R` &rarr; Analysis and plot of Fig. S1.


#### Unit combinations and response times. Figures 3, 4, S2-S4

`combos_lvr.R` &rarr; Computing the LvR for combinations of units. Production file.  
`combos_correlation_rptime_lvr.R` &rarr; Correlations between response times and LvR (Fig. 4a). Production file.  
`combos_correlation_rptime_frate.R` &rarr; Correlation between response times and firing rates and partial correlation with LvR (Fig. 4b). Production file.  
`combos_kstest_setsize_correct_lvr.R` &rarr; KS-like test to check for relationships between LvR and set sizes or correct responses (Figs. S3 and S4). Production file.  

`combos_figure_3.R` &rarr; Analysis and plot of Fig. 3.  
`combos_figure_4.R` &rarr; Analysis and plot of Fig. 4.  
`combos_figure_S2.R` &rarr; Analysis and plot of Fig. S2.  
`combos_figure_S3_S4.R`  &rarr; Analysis and plot of Figs. S3, S4.  


#### Population bursts and Fano factor. Figures 5, S5-S8

`trajectory_creation.R` &rarr; Convolution of spike times with gaussian kernel. More general options are however available. Production file.  

`population_burst_identification.R` &rarr; Identification of population bursts and their unit composition. Production file.  
`population_burst_figure_5ab_S5_S7.R` &rarr; Analysis of Figs. 5ab, S5, S7. Plot of Figs. S5, S7.  
`population_burst_figure_S6_S8.R` &rarr; Analysis and plot of Figs. S6, S8.  

`fano_factor_calculation.R` &rarr; Calculation of Fano factors and related quantities. Production file.  
`fano_factor_figure_5cd.R` &rarr; Analysis of Figs. 5c-d.  
`fano_factor_figure_5e.R` &rarr; Analysis of Fig. 5e.  

`assembly_figure_5_S7.R` &rarr; Assembly and plot of figure 5 panels (from `population_burst_figure_5ab*` and `fano_factor_figure*`) and figure S7.  


#### Decoding analysis. Figures 6, S9.

`decoder_calculation.R` &rarr; Performing decoding analysis. Production file.  

`decoder_figure_6c.R` &rarr; Analysis of Fig. 6c.  
`decoder_figure_6_S9.R` &rarr; Analysis of Figs. 6ab, S9. Plot of Fig. 6, S9.  


####  Utils

`utils.R` &rarr; General utils.  
`utils_project.R` &rarr; Specific to this project.  
`utils_bursty.R` &rarr; Tools for burstiness measures and other quantities.  

