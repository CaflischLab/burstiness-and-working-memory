## Script for computing the decoding-related quantities. Relevant for Figure 6ab, S9. Production files. 

library(here)
library(parallel)
library(tidyverse)
library(reticulate)

source(here("R/utils.R"))
source(here("R/utils_project.R"))
source(here("R/utils_bursty.R"))

## Use a Conda environment with `scikit-learn` installed. Used for the SVM
use_condaenv("conda_env", required = T)

ncores <- 5

event_levels <- c("Fixation", "Stimulus", "Maintenance", "Probe")

## #############
## Preamble: reading spike data

main <- readRDS(here('data/processed/Allsessions_data.RData'))

sess_frate <- frate_selection(frate_threshold = 1) %>%
    select(what, ends_with("On")) %>%
    filter(rowSums(across(c(where(is.numeric), -Tot_On), ~ . >= 5)) > 0) # at least a brain region with minimum 5 neurons

cat("Selecting the neurons\n")
sel <- main %>%
    filter(!artifact) %>%
    filter(what %in% sess_frate$what) %>%
    mutate_at("unit", map,
              ~ mutate(.x, unit_id = str_replace(unit_id, '_(?=[0-9]+$)', paste0('_', str_sub(mtl,1,1))))) %>%
    remove_lowneurons(frate_threshold = 1) %>%
    remove_crosstalk() %>%
    unnest(unit) %>%
    mutate_at("spike_time", map, ~ sort(.x)) %>%
    arrange(what, trial, unit_id)  %>%
    select(-tags, -artifact, -unit_number)

## After suggestion from Sarnthein meeting, testing the correctness in Encoding period


## ######
## Setting the SVM

nfolds <- 10L
sk_cv <- import("sklearn.model_selection") # For cross-validation
sk_svm <- import("sklearn.svm") 
sk_model <- sk_svm$SVC(C = 1.0,
                       kernel = "linear",
                       ## gamma = "scale",
                       shrinking = F,
                       probability = FALSE,
                       class_weight = "balanced",
                       max_iter = 50000L,
                       verbose = F)


## ##########
## Preparing the data needed for the analysis

## General parameters
step <- 0.25 # Time step & window size
nbootstrap <- 50 # Number of bootstraps

## Labels of the 250ms windows 
event_breaks <- seq(-6, 0, by = step)
event_labels <- tibble(name = rep.int(event_levels[1:3], c(1, 2, 3) * (1 / step)), lt = 1) %>%
    group_by(name) %>%
    mutate(across(lt, ~ ifelse.c(n() > 1, cumsum(.), .))) %>%
    ungroup() %>%
    str_glue_data("{name}_{lt}") %>% as.chr()

## Computation of LvR and replacement of NA values with mean 
lvr_df <- sel %>%
    mutate(lvr = map_dbl(spike_time, ~ lvr(.x, min_isi = 3, default = NA))) %>%
    select(trial, mtl, unit_id, lvr) %>%
    group_by(unit_id) %>%
    mutate(lvr = ifelse(is.na(lvr), mean(lvr, na.rm = T), lvr)) %>%
    ungroup() 

## Compute the firing rates in bin of 250ms;
frate_250ms_perQuartile <- sel %>%
    mutate(spike_time = map2(spike_time, response_time, function(st, rpt) {
        st <- st[st < rpt]
        ## For probe period only two bins of 250ms are taken
        brk_probe <- ifelse.c(rpt > 2,
                              seq(2 - 0.5, 2, by = step),
                              seq(rpt - 0.5, rpt, by = step))
        if(brk_probe[1] <= 0) stop("Intersection of breaks")
        ## Distributing the spike times in each of the 250ms bins
        fct <- cut(st,
                   breaks = c(event_breaks, brk_probe),
                   labels = c(event_labels, "Dumm", paste0("Probe_", seq(lt(brk_probe) - 1))),
                   include.lowest = T)
        split(st, fct, drop = F)
    })) %>%
    unnest(spike_time) %>%
    mutate(event = names(spike_time), .before = 'event') %>%
    filter(event != "Dumm") %>%
    mutate(across(spike_time, unname)) %>%
    mutate(frate = map_dbl(spike_time, ~ lt(.x))) %>% ## Firing rate within each 250ms bin
    mutate(across(c(set_size, correct, match), ~ fns_sel[[cur_column()]](.))) %>%
    mutate(across(set_size, ~ fct_collapse(., "68" = c("6", "8")))) %>%
    mutate(time = as.numeric(fct_inorder(event))) %>%
    mutate(across(event, ~ factor(str_remove(., "_[0-9]+$"), levels = event_levels))) ## Remove the bin id 


## ###################
## Decoding of Figure 6a, S8a. Here the partition into quartile is done by evaluating the mean LvR of each unit within each bootstrap.
## Thus boot per boot the pseudo population can vary.

## Parameters
nquartile <- 4 
nchance <- 500 # Number of Random permutation for creating chance levels

## What to decode, Where to compute
decode_options <- tribble(~ to_decode, ~ event,
                     "set_size", "Maintenance",
                     "correct", "Maintenance"
                     ) %>%x
    mutate(mtl = list(c("All", "Hipp", "Amyg", "Ent")),
           quartile = list(c("qall", paste0("q", seq(nquartile))))) %>%
    unnest(col = mtl) %>%
    unnest(col = quartile) %>%
    rowwise() %>%
    split.c()

## Decoder analysis
decode_df <- imap_dfr(decode_options, function(ls, idx) {

    ## Options
    ls <- ungroup(ls)
    print(unlist(ls))
    
    ## First filtering based to the `ls` options
    filtered <- frate_250ms_perQuartile %>%
        filter(str_detect(mtl, ifelse(ls$mtl == "All", "", ls$mtl))) %>%
        filter(event == ls$event)
    if(ls$to_decode == "set_size") {
        ## Only correct trials when decoding set_size!
        filtered <- filter(filtered, correct == "correct")
    }
    if(anyNA(frate_250ms_perQuartile[[ls$to_decode]])) stop("NA Found")
    
    
    ## Sessions having the minimum number of trials (5) per `to_decode` level
    nlevels <- n_distinct(frate_250ms_perQuartile[[ls$to_decode]])
    surviving <- filtered %>%
        distinct(what, trial, across(ls$to_decode)) %>%
        count(what, across(ls$to_decode), name = "ntrials") %>%
        nest(tmp = -what) %>%
        mutate(tmp = map(tmp, function(df) {
            ifelse.c(nrow(filter(df, ntrials >= 5)) == nlevels, df, NULL)
        })) %>%
        unnest(tmp)
    
    ## Trials to be taken per each bootstrap
    ntrials <- min(surviving$ntrials)
    cat("Minumum trials are", ntrials, "\n")
    if(ntrials < 5) stop("Too few trials")
    
    ## Filtering the sessions passing the threshold
    ## Z-scoring the firing rates
    filtered <- filtered %>%
        filter(what %in% unique(surviving$what)) %>%
        select(what, trial, unit_id, time, frate, all_of(ls$to_decode)) %>%
        group_by(what, unit_id) %>%
        mutate_at("frate", function(x) {
            x <- as.vector(scale(x)) ## Zscoring
            if(anyNA(x)) stop("NA FOUND")
            return(x)
        }) %>%
        ungroup()

    cat("Number of units is", n_distinct(filtered$unit_id), "\n")
    cat("--------\n\n")
    
    RNGkind("L'Ecuyer-CMRG")
    set.seed(1000 + as.numeric(idx))
    tictoc::tic()
    
    ## Cycling over the different bootstrap
    decoding <- map(seq(nbootstrap), function(dumm) {
        ## Sampling of the trials
        trials_tobesel <- filtered %>%
            distinct(what, across(all_of(ls$to_decode)), trial) %>%
            group_by(what, across(all_of(ls$to_decode))) %>%
            sample_n(size = ntrials, replace= F) %>%
            ungroup()

        ## Filtering the dataset on the selected trials
        final <- filtered %>%
            select(what, trial, time, unit_id, all_of(ls$to_decode), frate) %>%
            right_join(trials_tobesel, by = c("what", "trial", ls$to_decode)) %>%
            left_join(lvr_df, by = c("trial", "unit_id"))

        ## LvR Quartile computation
        quartile_df <- final %>%
            distinct(trial, unit_id, lvr) %>%
            group_by(unit_id) %>%
            summarize(across(lvr, mean), .groups = "drop") %>%
            mutate(quartile = ntile(lvr, nquartile)) %>%
            mutate(across(quartile, ~ paste0("q", .)))

        ## Identification of the units in the given quartile
        unit_sel <- ifelse.c(ls$quartile == "qall",
                             quartile_df$unit_id,
                             filter(quartile_df, quartile == ls$quartile)$unit_id) %>%
            sort()

        ## Preparing the correct input format to sklearn 
        sk_input <- final %>%
            filter(unit_id %in% unit_sel) %>%
            group_by(what, across(all_of(ls$to_decode))) %>%
            mutate(trial = plyr::mapvalues(trial,
                                           unique(trial),
                                           paste0("trial_", seq(ntrials)))) %>%
            ungroup() %>%
            select(-what, -lvr) %>%
            pivot_wider(id_cols = c(ls$to_decode, "trial", "time"),
                        names_from = "unit_id", values_from= "frate") %>%
            arrange(across(all_of(ls$to_decode)), trial, time)
        x_input <- as.matrix(select(sk_input, matches("[0-9]{2}_[0-9]{2}")))
        
        ## Decoding with cross-validation: True value
        cv_results <- sk_cv$cross_validate(estimator = sk_model,
                                           X = unname(x_input),
                                           y = as.chr(sk_input[[ls$to_decode]]),
                                           cv = nfolds,
                                           return_estimator = T)

        ## Extracting the relevant results
        coef <- map(cv_results$estimator, ~ .x$coef_ %>%
                                              `colnames<-`(colnames(x_input)))
        scores <- cv_results$test_score
        
        ## Chance level computation
        tictoc::tic()
        chance <- mclapply(mc.set.seed = T, mc.cores = ncores, X = seq(nchance), function(df_ls) {
            rand_idx <- sample(seq(nrow(sk_input)))
            sk_cv$cross_val_score(sk_model,
                                  unname(x_input),
                                  as.chr(sk_input[[ls$to_decode]])[rand_idx],
                                  cv = nfolds)
        }, mc.preschedule = T) %>%
            do.call("rbind", .)
        tictoc::toc()
        
        ## Output
        return(list(scores = scores, chance = chance, coef = coef, trials = trials_tobesel))
    }) ## End of loop over the bootstraps
    tictoc::toc()
    
    if(any(map_lgl(decoding, inherits, what = "try-error"))) {
        stop("Error found during encoding")
    }

    ## List of the trials included in each single boot
    trial_boot <- map(decoding, "trials") %>%
        set_names(~ paste0("boot", seq(nbootstrap))) %>%
        bind_rows(.id = "boot")
    
    ## Better format for the results
    decoding <- decoding %>%
        transpose() %>%
        map_at("scores", ~ do.call('rbind', .x)) %>%
        map_at("chance", ~ unname(abind::abind(.x, along = 3)) %>%
                             aperm(c(3, 1, 2))) %>%
        map_at("coef", function(cf) {
            names(cf) <- paste0("boot", seq(nbootstrap))
            cf <- map(cf, ~ do.call("rbind", .x)) 
            return(cf)
        }) %>%
        map_at("trials", ~ NULL) %>%
        compact()
    cat("\n")

    ## Output
    return(add_column(ls, decoding = list(decoding), trial_boot = list(trial_boot)))
})


name_file <- here("data/processed",
                  str_glue("Decoding_frate_LvR{nquartile}Quartile_PerBin{1000*step}ms_NoBlocks_ZScored_SVMlinear_10folds.RData"))
saveRDS(decode_df, name_file)
cat("Saved", name_file)
rm(decode_df)



## ###################
## Decoding by removal on n-tiles of Figures 6b, S8b. Same considerations as above.

## Parameters
ntiles <- 20 ## How many n-tiles

## What to decode, Where to compute
decode_options <- tribble(~ to_decode, ~ event,
                     "set_size", "Maintenance",
                     "correct", "Maintenance"
                     ) %>%x
    mutate(mtl = list(c("Hipp", "Amyg", "Ent", "All"))) %>%
    unnest(col = mtl) %>% 
        rowwise() %>%
        split.c()

## This is for quartile analysis
tictoc::tic()
decode_df <- imap_dfr(decode_options, function(ls, idx) {

    ## Options
    ls <- ungroup(ls)
    print(unlist(ls))

    ## First filtering based on the options in `ls`
    filtered <- frate_250ms_perQuartile %>%
        filter(str_detect(mtl, ifelse(ls$mtl == "All", "", ls$mtl))) %>%
        filter(event == ls$event)
    if(ls$to_decode == "set_size") {
        ## Only correct trials when decoding set_size!
        filtered <- filter(filtered, correct == "correct")
    }
    if(anyNA(frate_250ms_perQuartile[[ls$to_decode]])) stop("NA Found")
    
    ## Sessions having the minimum number of trials (5) per `to_decode` level
    nlevels <- n_distinct(frate_250ms_perQuartile[[ls$to_decode]])
    surviving <- filtered %>%
        distinct(what, trial, across(ls$to_decode)) %>%
        count(what, across(ls$to_decode), name = "ntrials") %>%
        nest(tmp = -what) %>%
        mutate(tmp = map(tmp, function(df) {
            ifelse.c(nrow(filter(df, ntrials >= 5)) == nlevels, df, NULL)
        })) %>%
        unnest(tmp)
    
    ## Trials to be taken per each bootstrap
    ntrials <- min(surviving$ntrials)
    cat("Minumum trials are", ntrials, "\n")
    if(ntrials < 5) stop("Too few trials")

    ## Filtering the sessions passing the threshold
    ## Z-scoring the firing rates
    filtered <- filtered %>%
        filter(what %in% unique(surviving$what)) %>%
        select(what, trial, unit_id, time, frate, all_of(ls$to_decode)) %>%
        group_by(what, unit_id) %>%
        mutate_at("frate", function(x) {
            x <- as.vector(scale(x)) ## Zscoring
            if(anyNA(x)) stop("NA FOUND")
            return(x)
        }) %>%
        ungroup()
    
    ## Cycling over the different bootstrap
    RNGkind("L'Ecuyer-CMRG")
    set.seed(1000 + as.numeric(idx))
    decoding <- mclapply(mc.set.seed = T, mc.cores = ncores, seq(nbootstrap), function(dumm) {

        ## Sampling of trials
        trials_tobesel <- filtered %>%
            distinct(what, across(all_of(ls$to_decode)), trial) %>%
            group_by(what, across(all_of(ls$to_decode))) %>%
            sample_n(size = ntrials, replace= F) %>%
            ungroup()

        ## Filtering the dataset on the selected trials
        final <- filtered %>%
            select(what, trial, time, unit_id, all_of(ls$to_decode), frate) %>%
            right_join(trials_tobesel, by = c("what", "trial", ls$to_decode)) %>%
            left_join(lvr_df, by = c("trial", "unit_id"))
        
        ## LvR Quartile computation
        quartile_df <- final %>%
            distinct(trial, unit_id, lvr) %>%
            group_by(unit_id) %>%
            summarize(across(lvr, mean), .groups = "drop") %>%
            mutate(quartile = ntile(lvr, ntiles)) %>%
            mutate(across(quartile, ~ paste0("q", .))) 

        ## Preparing the correct input format to sklearn 
        sk_input <- final %>%
            group_by(what, across(all_of(ls$to_decode))) %>%
            mutate(trial = plyr::mapvalues(trial,
                                           unique(trial),
                                           paste0("trial_", seq(ntrials)))) %>%
            ungroup() %>%
            select(-what, -lvr) %>%
            pivot_wider(id_cols = c(ls$to_decode, "trial", "time"),
                        names_from = "unit_id", values_from= "frate") %>%
            arrange(across(all_of(ls$to_decode)), trial, time)

        ## Loop over the different ntiles to be removed 
        output <- map.c(c("qall", paste0("q", seq(ntiles))), function(qq) {
            ## Units selected
            unit_sel <- ifelse.c(qq == "qall",
                                 quartile_df$unit_id,
                                 filter(quartile_df, quartile != qq)$unit_id) %>%
                sort()

            ## Correct input format
            x_input <- as.matrix(select(sk_input, all_of(unit_sel)))

            ## Decoding: True level
            cv_results <- sk_cv$cross_validate(estimator = sk_model,
                                               X = unname(x_input),
                                               y = as.chr(sk_input[[ls$to_decode]]),
                                               cv = nfolds, ## It is stratified by default
                                               return_estimator = T)
            ## Extracting relevant information
            coef <- map(cv_results$estimator, ~ .x$coef_ %>%
                                                  as.vector() %>%
                                                  setNames(unit_sel)) %>%
                { do.call("cbind", .) }
            scores <- cv_results$test_score

            ## Output
            return(list(scores = scores, coef = coef))
        }) # End loop over the n-tiles
    }) # End loop over bootstraps
    
    if(any(map_lgl(decoding, inherits, what = "try-error"))) {
        stop("Error Found duing decoding")
    }
    
    ## Better format
    decoding <- decoding %>%
        set_names(~ paste0("boot", str_pad(seq(nbootstrap), width = 3, side= 'left', pad = 0))) %>%
        transpose() %>%
        map(transpose) %>%
        map(~ map_at(.x, "scores", function(xx) do.call('rbind', xx))) %>%
        map(~ map_at(.x, "coef", function(xx) {
            enframe(xx, name = "boot", value = "coef")
        })) %>%
        enframe(name = "quartile_removed") %>%
        unnest_wider(value)

    ## Output
    return(add_column(ls, decoding = list(decoding)))
})
tictoc::toc()

## Saving
name_file <- here("data/processed",
                  str_glue("Decoding_frate_LvR_wo{ntiles}Quartile_PerBin{1000*step}ms_NoBlocks_ZScored_SVMlinear_10folds.RData"))
saveRDS(decode_df, name_file)
cat("Saved", name_file)
