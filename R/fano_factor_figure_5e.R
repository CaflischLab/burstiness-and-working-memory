## Analysis of Figure 5e

library(here)
library(parallel)
library(RColorBrewer)
library(tidyverse)
library(cowplot)
library(patchwork)

source(here("R/utils.R"))
source(here("R/utils_project.R"))
source(here("R/utils_bursty.R"))

theme_set(cowplot::theme_cowplot())

event_breaks <- c(-6, -5, -3, 0, 2)
anatom_levels <- c("Hipp", "Amyg", "Ent")


## ################
## PLOTS DRAFT: Fano factor per Set size

## Reading Fano factors, formatting and smoothing data
fano_set <- map.c(c("not_align", "align"), function(align) {
    align_lab <- ifelse(align == "align", "_alignResponseTime", "")
    df <- expand_grid(mtl = anatom_levels,
                      fact = c("4", "68")) %>%
        mutate(data = map2(mtl, fact,
                           ~ readRDS(here("data/processed", str_glue("Fano_MeanMatchedBalanced_mtl{.x}_window500ms_set{.y}{align_lab}.RData"))))) %>%
        unnest(data) %>%
        select(-data, -distr, -fano_boot, -perc_left) %>%
        mutate(across(mtl, factor, levels = anatom_levels)) %>% 
        arrange(mtl, fact, time) %>%
        group_by(mtl, fact) %>%
        mutate(across(c(estimate, lower_ci, upper_ci),
                      ~ zoo::rollmean(., k = 3,
                                      align = "left",
                                      fill = NA))) %>%
        ungroup()
    df <- mutate(df, across(fact, factor, levels = c("4", "68"), labels = c("4", "6-8"))) 
    return(df)
})

## Plot options
palette <- setNames(rcartocolor::carto_pal(7, name = "Geyser")[c(5, 2)], c("4", "6-8"))
ylim <- c(0.81, 1.4)

## Plot: left panels
pl_set1 <- fano_set[["not_align"]] %>%
    filter(time != -6) %>%
    ggplot(aes(time, estimate)) +
    geom_vline(xintercept = event_breaks[c(-1, -5)], alpha = .5, linetype = 2) +
    geom_ribbon(aes(ymin = lower_ci, ymax = upper_ci,
                    fill = fact),
                alpha = 0.1) +
    geom_line(aes(color = fact), size = 1.2) +
    facet_grid(mtl ~ ., switch = "y") +
    scale_color_manual(name = "Set size",
                       values = palette,
                       aesthetics = c("color", "fill")) +
    geom_vline(data= tibble(mtl = fct_inorder(anatom_levels),
                            xintercept = -6.05),
               aes(xintercept = xintercept), size = 2,
               alpha = 0.7,
               color = brewer.pal(3, "Set1")[c(2, 1, 3)]) +
    scale_x_continuous("Time from probe [s]",
                       breaks = c(-5, -3, 0, 2), 
                       ## limits = c(-6, 2),
                       expand = expansion(add = c(0.1, 0.1)))  +
    scale_y_continuous("Fano factor", expand = expansion(mult = 0),
                       sec.axis = dup_axis(),
                       position = "right") +
    coord_cartesian(ylim = ylim,
                    xlim = c(-6, 2), # This focuses the x-axis on the range of interest
                    clip = 'off') +
    theme_cowplot(font_size = 18) +
    theme(axis.title.y.right = element_blank(),
          axis.line.y.right = element_blank(),
          axis.text.y.right = element_blank(),
          strip.background = element_blank(),
          strip.text = element_blank())

## Plot: right panels
pl_set2 <- fano_set[["align"]] %>%
    filter(time != -2) %>%
    ggplot(aes(time, estimate)) +
    geom_vline(xintercept = 0, alpha = .5, linetype = 2) +
    geom_ribbon(aes(ymin = lower_ci, ymax = upper_ci,
                    fill = fact),
                alpha = 0.1) +
    geom_line(aes(color = fact), size = 1.2) +
    facet_grid(mtl ~ .) +
    scale_color_manual(name = legend_name,
                       values = palette,
                       aesthetics = c("color", "fill")) +
    geom_vline(data= tibble(mtl = fct_inorder(anatom_levels),
                            xintercept = 0.05),
               aes(xintercept = xintercept), size = 2,
               alpha = 0.7,
               color = brewer.pal(3, "Set1")[c(2, 1, 3)]) +
    scale_x_continuous("Time from response [s]",
                       breaks = c(-2, 0), 
                       expand = expansion(add = c(0.1, 0.1)))  +
    scale_y_continuous("Fano factor", expand = expansion(mult = 0), position = "left") +
    coord_cartesian(ylim = ylim,
                    xlim = c(-2, 0), # This focuses the x-axis on the range of interest
                    clip = 'off') +
    theme_cowplot(font_size = 18) +
    theme(axis.title.y = element_blank(),
          axis.line.y = element_blank(),
          strip.background = element_blank(),
          strip.text = element_text(size = 18))
pl_fanoSet <- patchwork::wrap_plots(list(pl_set1, pl_set2), nrow = 1, widths = c(1, 0.27), guides = "collect")

## Saving
plot_fct <- list(both = pl_fanoSet, pl1 = pl_set1, pl2 = pl_set2))
saveRDS(plot_fct, here("plots/RData/FanoFactor_setSize4-68.RData"))
