## Anova test of Figure 2

library(here)
library(tidyverse)
library(cowplot)
library(RColorBrewer)
library(patchwork)
theme_set(cowplot::theme_cowplot())

source(here("R/utils.R"))
source(here("R/utils_project.R"))
source(here("R/utils_bursty.R"))

event_levels <- c("Fixation", "Stimulus", "Maintenance", "Probe")
anatom_levels <- c("Hipp", "Amyg", "Ent")
    

## ####################
## Preamble: Reading data

main <- readRDS(here('data/processed/Allsessions_data.RData'))

sess_frate <- frate_selection(frate_threshold = 1) %>%
    select(what, ends_with("On")) %>%
    filter(rowSums(across(c(where(is.numeric), -Tot_On), ~ . >= 5)) > 0) # at least a brain region with minimum 5 neurons

sel <- main %>%
    filter(!artifact) %>%
    filter(what %in% sess_frate$what) %>%
    mutate_at("unit", map,
              ~ mutate(.x, unit_id = str_replace(unit_id, '_(?=[0-9]+$)', paste0('_', str_sub(mtl,1,1))))) %>%
    remove_lowneurons(frate_threshold = 1) %>%
    remove_crosstalk() %>%
    unnest(unit) %>%
    mutate_at("spike_time", map, ~ sort(.x)) %>%
    arrange(what, trial, unit_id)  %>%
    select(-tags, -artifact, -unit_number) 


## ####################
## ANOVA tests

## Reading the LvR values
single_df <- readRDS(here("data/processed", str_glue("LvR_single_Nocombos_minisi3_window2s_betterformat.RData")))

## Filtering the trials
time_labs <- c(-3, -0.5, 1.95)
lvr_df <- single_df %>%
    left_join(distinct(sel, what, trial, set_size, correct),
              by = c("what", 'trial')) %>%
    mutate(across(c(set_size, correct), ~ fns_sel[[cur_column()]](.))) %>%
    mutate(time = as.numeric(str_extract(time, "(?<=_).*"))) %>%
    filter(near(time, time_labs[1]) | near(time, time_labs[2]) | near(time, time_labs[3])) %>%
    mutate(event =  factor(as.chr(round(time, digits = 2)),
                           levels = as.chr(time_labs),
                           labels = event_levels[-1]),
           .after = "time") %>%
    mutate(toolong = factor(correct == "wrong" | response_time > 2,
                            levels = c(T, F),
                            labels = c('slow', 'fast'))) %>%
    drop_na(lvr)

## Identifying the sessions with at least 5 wrong trials
sess_correct <- distinct(sel, what, trial, correct) %>%
    group_by(what) %>%
    count(correct) %>%
    ungroup() %>%
    filter(correct == 0) %>%
    filter(n >= 5) %>%
    pull(what)

## Classic ANOVA Type II on the full model (i.e., including `correct` factor)
anova_full <- lvr_df %>%
    filter(what %in% sess_correct) %>%
    nest(data = -mtl) %>%
    arrange(mtl) %>%
    mutate(across(data, map, function(df) {
        ## lm_mod <- lm(lvr ~ set_size * correct * event, data = df)
        lm_mod <- lm(lvr ~ set_size + correct + event, data = df)
        aov_mod <- car::Anova(lm_mod, type = "II")
        sign_df <- broom::tidy(aov_mod) %>%
            filter(p.value < 0.05)
        if(nrow(sign_df) != 0) {
            tukey <- TukeyHSD(aov(lm_mod), which = sign_df$term)
            pair_t <- map.c(sign_df$term, ~ pairwise.t.test(df$lvr, g = df[[.x]]))
        } else {
            tukey <- NULL
            pair_t <- NULL
        }
        return(list(lm = lm_mod,
                    anova = aov_mod,
                    tukey = tukey,
                    pair_t = pair_t))
    })) %>%
    deframe()

map(anova_full, 'tukey')
map(anova_full, 'anova')
map(anova_full, 'pair_t')


## Classic ANOVA Type II without `correct` factor and only on correct responses
anova_nocorrect <- lvr_df %>%
    filter(correct == 'correct') %>%
    nest(data = -mtl) %>%
    arrange(mtl) %>%
    mutate(across(data, map, function(df) {
        ## lm_mod <- lm(lvr ~ set_size * correct * event, data = df)
        df <- df %>%
            mutate(across(c(event, set_size), fct_rev))
        lm_mod <- lm(lvr ~ set_size * event, data = df)
        aov_mod <- car::Anova(lm_mod, type = "II")
        sign_df <- broom::tidy(aov_mod) %>%
            filter(p.value < 0.05)
        cat(sign_df$term, "\n")
        if(nrow(sign_df) != 0) {
            tukey <- TukeyHSD(aov(lm_mod), which = sign_df$term) %>%
                broom::tidy() 
            pair_t <- map.c(sign_df$term,
                            ~ broom::tidy(pairwise.t.test(df$lvr, g = df[[.x]]))) %>%
                bind_rows(.id = 'term') %>%
                unite(col = 'contrast', group1, group2, sep = '-') %>%
                rename("adj.p.value" = "p.value")
        } else {
            tukey <- NULL
            pair_t <- NULL
        }
        return(list(lm = lm_mod,
                    anova = aov_mod,
                    tukey = tukey,
                    pair_t = pair_t))
    })) %>%
    deframe()


## Print the anova and effect size results
map(anova_nocorrect, 'anova') %>%
    map(~ list(.x, effectsize::omega_squared(.x, partial=F)))


## Colour Palette
palette_setsize <- c(rcartocolor::carto_pal(7, name = "Geyser")[c(6, 2)],
                     rcartocolor::carto_pal(7, name = "Fall")[c(1)]) %>%
    setNames(c("4", "6", "8"))

## Adjusting the data.frame of anova results for plotting
df_aov <- map(anova_nocorrect, 'pair_t') %>%
    bind_rows(.id = "mtl") %>%
    select(mtl, feat = term, contrast , p= `adj.p.value`) %>%
    mutate(across(contrast, str_replace, "Maintenance", "Maint")) %>%
    mutate(across(contrast, str_replace, "Probe", "Retrv")) %>%
    mutate(across(contrast, str_replace, "Stimulus", "Encod")) %>%
    rowwise() %>%
    mutate(label = str_glue("{contrast}: p={signif(p, 3)}{pvalue_to_star(p)}")) %>%
    ungroup() %>%
    group_by(feat, mtl) %>%
    summarize(across(label, ~ paste0(rev(.x), collapse = '\n')), .groups ='drop') %>%
    mutate(across(mtl, fns_sel[["mtl"]])) %>%
    arrange(mtl) %>%
    add_column(y = c(1.22, 1.37, 1.35))

## Summarizing the LvR results 
lvr_df <- lvr_df %>%
    group_by(mtl, event, set_size) %>%
    summarize(across(lvr, list(mean = mean, sem = sem)), .groups = 'drop') %>%
    mutate(across(event, fns_sel[['event_plot']]))

## Plot
pl_anova <- ggplot(lvr_df, aes(event, y = lvr_mean)) +
    geom_col(aes(fill = set_size), position = position_dodge()) +
    geom_linerange(aes(ymin = lvr_mean - lvr_sem,
                       ymax = lvr_mean + lvr_sem,
                       group = set_size), alpha = 0.9,
                   position = position_dodge(width = 0.9), size = 1) +
    geom_text(data = df_aov, aes(y = y, label = label), vjust = 1, x = 0.7, hjust = 0,
              size = 5) +
    scale_fill_manual(values = palette_setsize, aesthetics = c("fill", "color")) +
    facet_grid(mtl ~ ., scales = "free_y") +
    scale_y_continuous(name = "LvR", n.breaks = 3) +
    coord_cartesian(ylim = c(1.1, NA)) +
    theme_cowplot(font_size = 18) +
    labs(fill = "Set size") +
    theme(strip.background = element_blank(),
          strip.text = element_text(size = 19),
          axis.title.x = element_blank(),
          legend.title = element_text(size = 17))

ggsave(plot = pl_anova, filename = here('plots/Figure_2.png'),
       bg = "white", type = 'cairo', width = 6.47, height =8.16)
