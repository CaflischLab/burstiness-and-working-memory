## Time series of population burst density are computed.
## Figure 5a, b, S5 and S7. Only the latter is saved as .png.
## The others are assembled in another file.

library(here)
library(parallel)
library(cowplot)
library(patchwork)
library(tidyverse)
library(RColorBrewer)
library(reticulate)

source(here("R/utils.R"))
source(here("R/utils_project.R"))
source(here("R/utils_bursty.R"))


burst_density <- function(df, ii, norm, brk) {
    ## Function to compute the population burst density
    ## Instrumental to bootstrap calculation `boot::boot`
    ## Unnest all of the time indexes of the bursts present and makes the histogram. Then appropriately normalizes (i.e., full number of trials)
    df %>%
        slice(ii) %>%
        filter(!map_lgl(burst_data, is.null)) %>%
        unnest(burst_data) %>%
        pull(time_idx) %>%
        flatten_dbl() %>%
        findInterval(vec = brk, left.open = F) %>%
        tabulate(nbins = lt(brk) - 1) %>%
        `/`(norm)
}


theme_set(theme_cowplot())
event_levels <- c("Fixation", "Stimulus", "Maintenance", "Probe")
event_breaks <- c(-6, -5, -3, 0, 2)
anatom_levels <- c("Hipp", "Amyg", "Ent")
event_duration <- c("Fixation" = 1, "Stimulus" = 2, "Maintenance" = 3, "Probe" = 2)


## ####################
## Preamble: Reading data

main <- readRDS(here('data/processed/Allsessions_data.RData'))

sess_frate <- frate_selection(frate_threshold = 1) %>%
    select(what, ends_with("On")) %>%
    filter(rowSums(across(c(where(is.numeric), -Tot_On), ~ . >= 5)) > 0) # at least a brain region with minimum 5 neurons

sel <- main %>%
    filter(!artifact) %>%
    filter(what %in% sess_frate$what) %>%
    mutate_at("unit", map,
              ~ mutate(.x, unit_id = str_replace(unit_id, '_(?=[0-9]+$)', paste0('_', str_sub(mtl,1,1))))) %>%
    remove_lowneurons(frate_threshold = 1) %>%
    remove_crosstalk() %>%
    select(what, trial, unit, set_size, response_time, correct) %>%
    mutate(across(c(set_size, correct), ~ fns_sel[[cur_column()]](.))) %>%
    unnest(unit) %>%
    mutate_at("spike_time", map, ~ sort(.x)) %>%
    select(-unit_number) %>%
    arrange(what, trial, unit_id) 


## ###################
## Figure 5a --> Just and example of population burst (not commented)

mtl_sel <- "Hipp"
pop_burst <- readRDS(here("data/processed/Population_bursts.RData"))

times_df <- sel %>%
    select(what, trial, mtl, unit_id, spike_time) %>%
    nest(times = -c(what, trial, mtl))
pl_example <- map(c(12, seq(20))[1], function(iseed) {
    set.seed(iseed)
    trial_prov <- pop_burst %>%
        filter(mtl == mtl_sel) %>%
        filter(!map_lgl(data_burst, is.null)) %>%
        mutate(ncell = map_dbl(data_burst, ~ unique(.x$ncell))) %>%
        filter(ncell > 15) %>%
        sample_n(1) %>%
        left_join(times_df, by = c("what", "trial", "mtl")) 
    series <- list.files(here("data/processed"), pattern = "-mtlAll-ftsFRUnit1Hz-krnPyNorm25ms-bin10ms.RData", full.names = T) %>%
        str_subset(what_id(trial_prov$what)) %>%
        readRDS() %>%
        remove_lowneurons_traj() %>%
        remove_crosstalk_traj() %>%
        select(what, trial, event, time, matches(str_sub(mtl_sel,1,1)))
    thres <- series %>%
        group_by(trial) %>%
        summarize(across(matches("feat_"), mean), .groups = "drop") %>%
        mutate(unit_mean = rowSums(across(matches("feat_"))),
               .keep = "unused") %>% pull(unit_mean) %>%
        { mean(.) + 3*sd(.)  }
    frate_global <- series %>%
        mutate(global = rowSums(across(matches("feat_"))),
               .keep = "unused") %>%
        filter(trial %in% trial_prov$trial) %>%
        select(time, global) %>%
        mutate(across(time, round, digits = 5))
    burst_df <- trial_prov$data_burst[[1]] %>%
        rowwise() %>%
        mutate(time = list(seq(start, end, by = 0.01))) %>%
        ungroup() %>%
        unnest(time) %>%
        mutate(ymin = thres) %>%
        mutate(across(time, round, digits = 5)) %>%
        left_join(frate_global, by = "time")
    pl_raster <- trial_prov %>%
        unnest(times) %>%
        select(unit_id, spike_time) %>%
        sample_frac(1) %>%
        mutate(across(unit_id, ~ as.numeric(fct_inorder(.)))) %>%
        unnest(spike_time) %>%
        ggplot(aes(y = unit_id, x = spike_time)) +
        ## geom_point() +
        geom_segment(aes(y = unit_id - 0.4, yend = unit_id + 0.4, xend = spike_time ), lineend = "round",
                     color = "grey30", alpha = 0.8) +
        geom_vline(xintercept = c(-5, -3, 0), linetype = 2, alpha = 0.5) +
        scale_x_continuous("",
                           breaks = c(-5, -3, 0),
                           minor_breaks = seq(-6, 2), 
                           limits = c(-6, 2), expand = expansion(add = c(0.1, 0.1))) +
        scale_y_continuous("Units", expand = expansion(mult = 0.01)) +
        coord_cartesian(clip = "off") +
        theme_cowplot(font_size = 18) +
        theme(legend.position = "none",
              axis.text.x = element_blank(),
              axis.line.x = element_blank(),
              axis.title.x = element_blank(),
              ## axis.ticks.x = element_line(size = 1),
              ## axis.ticks.length.x = unit(0.2, "cm")
              ) 
    pals <- rcartocolor::carto_pal(7, "SunsetDark")[c(3,1)]
    pl_frate <- frate_global %>%
        ggplot(aes(time, global)) +
        geom_vline(xintercept = c(-5, -3, 0), linetype = 2, alpha = 0.5) +
        geom_line() +
        geom_hline(yintercept = thres, color = pals[1], size = 1.1) +
        geom_ribbon(data = burst_df, aes(ymin = 0, ymax = global, group = burst_idx), fill = pals[2], alpha = 0.6) +
        scale_y_continuous("Firing\nrate [a.u.]",
                           n.breaks = 3,
                           expand = expansion(mult = c(0.01, 0))) +
        scale_x_continuous("Time from probe [s]",
                           breaks = c(-5, -3, 0),
                           minor_breaks = seq(-6, 2), 
                           limits = c(-6, 2), expand = expansion(add = c(0.1, 0.1))) +
        theme_cowplot(font_size = 18) +
        theme(plot.margin = margin(b = 10, t = 0, unit = "pt"),
              axis.title.x = element_text(size = 16))
    pl_tot <- plot_grid(pl_raster + theme(plot.margin = margin(b = 0, t = 10, unit = "pt")),
                        pl_frate,
                        rel_heights = c(1, .5), ncol =1, axis = "lr", align = "v")
    return(pl_tot)
})
saveRDS(file = here("plots/RData", "PopulationBurst_example.RData"), pl_example[[1]])



## ###################################
## TIME SERIES of population burst density (Figure 5b, S5, S7)

no_probe <- FALSE # Removing probe neurons. Set to TRUE for obtaining data of Fig. S7

## Reading population bursts
pop_burst <- readRDS(here("data/processed", str_glue("Population_bursts{ifelse(no_probe, \"_noprobe\",\"\")}.RData"))) %>%
    mutate(across(set_size, fct_collapse, `6-8` = c("6", "8")))
    
## Reading trials that were effectively investigated for population bursts
pop_burst_trial <- readRDS(here("data/processed", str_glue("Population_bursts_trialscheme{ifelse(no_probe, \"_noprobe\",\"\")}.RData"))) %>%
    left_join(distinct(sel, what, trial, set_size), by = c("what", "trial")) %>%
    mutate(across(set_size, fct_collapse, `6-8` = c("6", "8"))) 


## Computing the population density time series with bootstrap.
## Distinguishig mtl and set size in two separate calculations
df_series <- map(c.c("mtl", "set_size"), function(fact) {

    grp_fact <- unique(c(fact, "mtl"))
    df_burst <- unnest(pop_burst, data_burst) 

    ## Time series aligned to probe appearance (left panels) 
    nsess_all <- pop_burst_trial %>%
        count(across(all_of(grp_fact)), name = "n_sample")
    brk <- seq(-6.005, 2.005, by = 0.01)
    df_burst_boot <- df_burst %>%
        rowwise() %>%
        mutate(time_idx = list(seq(start, end, by = 0.01))) %>%
        ungroup() %>%
        select(all_of(grp_fact), trial, what, time_idx) %>%
        nest(burst_data = -c(all_of(grp_fact), trial, what)) %>%
        right_join(pop_burst_trial, by = c(grp_fact, "what", "trial")) %>%
        nest(fact_data = -all_of(grp_fact)) %>%
        left_join(nsess_all, by = grp_fact) %>%
        mutate(fact_data = map2(fact_data, n_sample, function(df, nn) {
            boot_burst <- boot::boot(df,
                                     statistic = burst_density,
                                     R = 1000, sim = "ordinary", stype = "i",
                                     norm = nn,
                                     brk = brk)
            tibble(time = brk[-1],
                   burst_mean = boot_burst[["t0"]],
                   burst_sem = apply(boot_burst[["t"]], 2, sem),
                   burst_q5 = apply(boot_burst[["t"]], 2, quantile, 0.025),
                   burst_q25 = apply(boot_burst[["t"]], 2, quantile, 0.25),
                   burst_q75 = apply(boot_burst[["t"]], 2, quantile, 0.75),
                   burst_q95 = apply(boot_burst[["t"]], 2, quantile, 0.975)
                   )
        })) %>%
        unnest(fact_data)
    cat(".")
    
    ## Time series aligned to response times (right panels) 
    nsess_align <- pop_burst_trial %>%
        left_join(distinct(sel,  what, trial, response_time), by = c("what", "trial")) %>%
        filter(response_time < 2) 
    ntrial_align <- nsess_align %>%
        count(across(all_of(grp_fact)), name = "n_sample") 
    brk_align <- seq(-8.005, 0.005, by = 0.01)
    df_burst_boot_align <- df_burst %>%
        mutate(across(c(start, end), ~ . - response_time)) %>%
        filter(response_time < 2) %>%
        rowwise() %>%
        mutate(time_idx = list(seq(start, end, by = 0.01))) %>%
        ungroup() %>%
        select(all_of(grp_fact), trial, what, time_idx) %>%
        nest(burst_data = -c(all_of(grp_fact), trial, what)) %>%
        right_join(nsess_align, by = c(grp_fact, "what", "trial")) %>% ## Adding missing trials (i.e. no burst)
        nest(fact_data = -all_of(grp_fact)) %>%
        left_join(ntrial_align, by = c(grp_fact)) %>%
        mutate(fact_data = map2(fact_data, n_sample, function(df, nn) {
            boot_burst <- boot::boot(df,
                                     statistic = burst_density,
                                     R = 1000, sim = "ordinary", stype = "i",
                                     norm = nn,
                                     brk = brk_align)
            tibble(time = brk_align[-1],
                   burst_mean = boot_burst[["t0"]],
                   burst_sem = apply(boot_burst[["t"]], 2, sem),
                   burst_q5 = apply(boot_burst[["t"]], 2, quantile, 0.025),
                   burst_q25 = apply(boot_burst[["t"]], 2, quantile, 0.25),
                   burst_q75 = apply(boot_burst[["t"]], 2, quantile, 0.75),
                   burst_q95 = apply(boot_burst[["t"]], 2, quantile, 0.975)
                   )
        })) %>%
        unnest(fact_data)

    return(list(not_align = df_burst_boot,
                align = df_burst_boot_align))
})

## Saving
saveRDS(df_series, here("data/processed", str_glue("Population_burst_countTimeSeries_bootstrapped{ifelse(no_probe, \"_noprobe\",\"\")}.RData")))



## ####################
## Figure 5b --> Time series of burst per MTL area

df_series <- readRDS(here("data/processed", "Population_burst_countTimeSeries_bootstrapped.RData"))

ylim <- c(0.06, 0.24)

## Left panel (not aligned to response times)
pl_series_mtl_notalign <- df_series[["mtl"]]$not_align %>%
    group_by(mtl) %>%
    mutate(across(matches("^burst"), zoo::rollmean, k = 10, align = "right", fill = 0)) %>%
    ungroup() %>%
    ggplot(aes(time, burst_mean)) +
    geom_vline(xintercept = c(-5, -3, 0), linetype = 2, alpha = 0.5) +
    geom_line(aes(color = mtl), size = 1.5) +
    ## geom_line(data = frate_trace, aes(y = frate, color = mtl), size = 1.5) +
    geom_ribbon(aes(ymin = burst_q25,
                    ymax = burst_q75, fill = mtl),
                alpha = 0.2) +
    ylab("Population burst density") + 
    scale_color_manual(name = "",
                       values = palette_fact$mtl,
                       aesthetics = c("fill", "color")) +
    scale_x_continuous("Time from probe [s]",
                       breaks = c(-5, -3, 0), minor_breaks = seq(-6, 2),
                       limits = c(-6, 1.95), expand = expansion(add = c(0.1, 0))) +
    scale_y_continuous(limits = ylim, expand = expansion(mult = c(0.01, 0))) +
    coord_cartesian(clip = "off") +
    ## coord_fixed(ratio = 25, ylim = ylim, xlim = c(-6, 2)) +
    theme_cowplot(font_size = 18) +
    theme(legend.position = "none",
          axis.title.x = element_text(size = 16))

## Right panel (aligned to response time)
pl_series_mtl_aligned <- df_series[["mtl"]]$align %>%
    group_by(mtl) %>%
    mutate(across(matches("^burst"), zoo::rollmean, k = 10, align = "right", fill = 0)) %>%
    ungroup() %>%
    ggplot(aes(time, burst_mean)) +
    geom_vline(xintercept = 0, alpha = 0.5, linetype = 2) +
    geom_line(aes(color = mtl), size = 1.5) +
    geom_ribbon(aes(ymin = burst_q25,
                    ymax = burst_q75,
                    fill = mtl),
                alpha = 0.2) +
    ylab("Population burst density") +
    scale_color_manual(name = "",
                       values = palette_fact$mtl,
                       aesthetics = c("fill", "color")) +
    scale_x_continuous("Time from response [s]", breaks = c(-2, 0), minor_breaks = c(-2, -1, 0),
                       limits = c(-2, 0.2), expand = expansion(add = 0.1)) +
    scale_y_continuous(limits = ylim, expand = expansion(mult = c(0.01, 0))) +
    coord_cartesian(clip = "off") +
    guides(color = guide_legend(override.aes = list(size = 2))) +
    theme_cowplot(font_size = 18) +
    theme(axis.title.y = element_blank(),
          axis.title.x = element_text(size = 16),
          ## axis.text.y = element_blank(),
          axis.line.y = element_blank())
pl_series_mtl <- plot_grid(pl_series_mtl_notalign,
                            pl_series_mtl_aligned + theme(legend.position = "none"),
                            ## get_legend(pl_series_aligned),
                            rel_widths = c(1, 1.4/4), align = "h", axis = "tb",
                            nrow = 1)

saveRDS(file = here("plots/RData",
                    str_glue("PopulationBurst_timeSeries_mtl{ifelse(no_probe, \"_noprobe\",\"\")}.RData")), pl_series_mtl)


## ######
## Figure S7, panel a --> Time series of burst per Set size

ylim <- c(0.02, 0.27)

## Left panel (not aligned to response times)
pl_series_set_notalign <- df_series[["set_size"]]$not_align %>%
    group_by(set_size, mtl) %>%
    mutate(across(matches("^burst"), zoo::rollmean, k = 10, align = "right", fill = 0)) %>%
    ungroup() %>%
    ggplot(aes(time, burst_mean)) +
    geom_vline(xintercept = c(-5, -3, 0), linetype = 2, alpha = 0.5) +
    geom_line(aes(color = set_size), size = 1.5) +
    geom_ribbon(aes(ymin = burst_q5,
                    ymax = burst_q95, fill = set_size),
                alpha = 0.2) +
    ylab("Population burst density") + 
    facet_grid(mtl ~ .) +
    geom_vline(data= tibble(mtl = fct_inorder(anatom_levels),
                            xintercept = -6.05),
               aes(xintercept = xintercept), size = 2,
               alpha = 0.7,
               color = brewer.pal(3, "Set1")[c(2, 1, 3)]) +
    scale_color_manual(name = "Set size",
                       values = palette_fact$set_size,
                       aesthetics = c("fill", "color")) +
    scale_x_continuous("Time from probe [s]",
                       breaks = c(-5, -3, 0), minor_breaks = seq(-6, 2),
                       expand = expansion(add = c(0.1, 0))) +
    scale_y_continuous(limits = ylim, expand = expansion(mult = c(0.01, 0))) +
    guides(color = guide_legend(override.aes = list(size = 2))) +
    coord_cartesian(clip = "off",
                    xlim = c(-6, 2)) +
    ## coord_fixed(ratio = 25, ylim = ylim, xlim = c(-6, 2)) +
    theme_cowplot(font_size = 18) +
    theme(axis.title.y.right = element_blank(),
          axis.line.y.right = element_blank(),
          axis.text.y.right = element_blank(),
          strip.background = element_blank(),
          strip.text = element_blank())

## Right panel (aligned to response time)
pl_series_set_aligned <- df_series[["set_size"]]$align %>%
    group_by(set_size, mtl) %>%
    mutate(across(matches("^burst"), zoo::rollmean, k = 10, align = "right", fill = 0)) %>%
    ungroup() %>%
    ggplot(aes(time, burst_mean)) +
    geom_vline(xintercept = 0, alpha = 0.5, linetype = 2) +
    geom_line(aes(color = set_size), size = 1.5) +
    geom_ribbon(aes(ymin = burst_q5,
                    ymax = burst_q95,
                    fill = set_size),
                alpha = 0.2) +
    ylab("Population burst density") +
    facet_grid(mtl ~ .) +
    geom_vline(data= tibble(mtl = fct_inorder(anatom_levels),
                            xintercept = 0.05),
               aes(xintercept = xintercept), size = 2,
               alpha = 0.7,
               color = brewer.pal(3, "Set1")[c(2, 1, 3)]) +
    scale_color_manual(name = "Set size",
                       values = palette_fact$set_size,
                       aesthetics = c("fill", "color")) +
    scale_x_continuous("Time from response [s]", breaks = c(-2, 0), minor_breaks = c(-2, -1, 0),
                       limits = c(-2, 0.2), expand = expansion(add = 0.1)) +
    scale_y_continuous(limits = ylim, expand = expansion(mult = c(0.01, 0))) +
    coord_cartesian(clip = "off") +
    guides(color = guide_legend(override.aes = list(size = 2))) +
    theme_cowplot(font_size = 18) +
    theme(axis.title.y = element_blank(),
          axis.line.y = element_blank(),
          strip.background = element_blank(),
          strip.text = element_text(size = 18))
pl_series_set <- wrap_plots(list(pl_series_set_notalign, pl_series_set_aligned),
                            nrow = 1, widths = c(1, 0.27), guides = "collect")

saveRDS(file = here("plots/RData", "PopulationBurst_timeSeries_setSize.RData"), pl_series_set)



## ################
## Figure S5: With probe AND W/O Probe neurons 

## Reading the time series. Both with probe neurons and not
df_probe_noprobe <- map(c("All" = F, "No probe" = T), function(prb) {
    df_series <- readRDS(here("data/processed",
                              str_glue("Population_burst_countTimeSeries_bootstrapped{ifelse(prb, \"_noprobe\",\"\")}.RData")))$mtl
}) %>%
    transpose() %>%
    map(bind_rows, .id = "probe")

ylim <- c(0.06, 0.24)

## Left panel (not aligned to response times)
pl_series_mtl_notalign <- df_probe_noprobe$not_align %>%
    group_by(mtl, probe) %>%
    mutate(across(matches("^burst"), zoo::rollmean, k = 10, align = "right", fill = 0)) %>%
    ungroup() %>%
    ggplot(aes(time, burst_mean)) +
    geom_vline(xintercept = c(-5, -3, 0), linetype = 2, alpha = 0.5) +
    geom_line(aes(color = probe), size = 1.5) +
    geom_ribbon(aes(ymin = burst_q25,
                    ymax = burst_q75, fill = probe),
                alpha = 0.2) +
    facet_grid(mtl ~ ., switch = "y") +
    ylab("Population burst density") + 
    scale_x_continuous("Time from probe [s]",
                       breaks = c(-5, -3, 0), minor_breaks = seq(-6, 2),
                       limits = c(-6, 1.95), expand = expansion(add = c(0.1, 0))) +
    scale_y_continuous(limits = ylim, expand = expansion(mult = c(0.01, 0))) +
    coord_cartesian(clip = "off") +
    ## coord_fixed(ratio = 25, ylim = ylim, xlim = c(-6, 2)) +
    theme_cowplot(font_size = 18) +
    theme(legend.position = "none",
          axis.title.x = element_text(size = 16),
          strip.placement = "outside",
          strip.background = element_blank(),
          strip.text = element_blank())

## Right panel (aligned to response time)
pl_series_mtl_aligned <- df_probe_noprobe$align %>%
    group_by(mtl, probe) %>%
    mutate(across(matches("^burst"), zoo::rollmean, k = 10, align = "right", fill = 0)) %>%
    ungroup() %>%
    ggplot(aes(time, burst_mean)) +
    geom_vline(xintercept = 0, alpha = 0.5, linetype = 2) +
    geom_line(aes(color = probe), size = 1.5) +
    geom_ribbon(aes(ymin = burst_q25,
                    ymax = burst_q75,
                    fill = probe),
                alpha = 0.2) +
    ylab("Population burst density") +
    facet_grid(mtl ~ .) +
    scale_x_continuous("Time from response [s]", breaks = c(-2, 0), minor_breaks = c(-2, -1, 0),
                       limits = c(-2, 0.2), expand = expansion(add = 0.1)) +
    scale_y_continuous(limits = ylim, expand = expansion(mult = c(0.01, 0))) +
    coord_cartesian(clip = "off") +
    guides(color = guide_legend(override.aes = list(size = 2))) +
    theme_cowplot(font_size = 18) +
    theme(axis.title.y = element_blank(),
          axis.title.x = element_text(size = 16),
          ## axis.text.y = element_blank(),
          axis.line.y = element_blank(),
          legend.title = element_blank(),
          legend.text = element_text(size = 18),
          strip.placement = "outside",
          strip.text = element_text(size = 20),
          strip.background = element_blank())

pl_series_probe_noprobe <- pl_series_mtl_notalign + pl_series_mtl_aligned +
    plot_layout(nrow = 1, widths = c(1, 1.2/4), guides = 'collect')

ggsave(here("plots/Figure_S5.png")),
       pl_series_probe_noprobe,
       width = 11, height = 7, bg = 'white', type ='cairo')

