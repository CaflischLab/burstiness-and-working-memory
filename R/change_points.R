## Identifying and saving the change points

library(here)
library(parallel)
library(tidyverse)

source(here("R/utils.R"))
source(here("R/utils_project.R"))
source(here("R/utils_bursty.R"))

## #######
## Preamble: readind data


main <- readRDS(here('data/processed/Allsessions_data.RData'))

sess_frate <- frate_selection(frate_threshold = 1) %>%
    select(what, ends_with("On")) %>%
    filter(rowSums(across(c(where(is.numeric), -Tot_On), ~ . >= 5)) > 0) # at least a brain region with minimum 5 neurons

sel <- main %>%
    filter(!artifact) %>%
    filter(what %in% sess_frate$what) %>%
    mutate_at("unit", map,
              ~ mutate(.x, unit_id = str_replace(unit_id, '_(?=[0-9]+$)', paste0('_', str_sub(mtl,1,1))))) %>%
    remove_lowneurons(frate_threshold = 1) %>%
    remove_crosstalk() %>%
    unnest(unit) %>%
    mutate_at("spike_time", map, ~ sort(.x)) %>%
    arrange(what, trial, unit_id)  %>%
    select(-tags, -artifact, -unit_number) 

## #######
## PRODUCTION: Change point analysis 

## Over all the sessions with adaptive approach of Jezzini, Mazzuccato 2013
tictoc::tic()
cp_df <- sel %>%
    mutate(cp = map(spike_time, ~ adaptive_cp_detector(.x))) #utils_bursty.R
tictoc::toc()

saveRDS(cp_df, here("data/processed/ChangePoint_binomial_adaptive.RData"))
