## Identification of population burst. Production file

library(here)
library(parallel)
library(tidyverse)

source(here("R/utils.R"))
source(here("R/utils_project.R"))
source(here("R/utils_bursty.R"))

event_levels <- c("Fixation", "Stimulus", "Maintenance", "Probe")
event_breaks <- c(-6, -5, -3, 0, 2)
anatom_levels <- c("Hipp", "Amyg", "Ent")
event_duration <- c("Fixation" = 1, "Stimulus" = 2, "Maintenance" = 3, "Probe" = 2)


## ####################
## Preamble: Reading data

type_unit <- list(maint = readRDS(here("data/processed/Maintenance_units.RData")),
                  probe = readRDS(here("data/processed/Probe_units.RData"))) %>%
    bind_rows(.id = "type") %>%
    select(-mtl) %>%
    pivot_wider(names_from = "type",values_from = "type") %>%
    mutate(across(c("maint", "probe"), ~ !is.na(.)))


## ###################
## Production: Identifying the population bursts

no_probe <- FALSE # Removing probe neurons. Set to TRUE for analysis of Fig. S5. FALSE otherwise.

## Time series with kernel
timeseries_files <- list.files(here("data/processed"),
                      pattern = "-mtlAll-ftsFRUnit1Hz-krnPyNorm25ms-bin10ms.RData",
                      full.names = T)

## Create the dataset with all the burst events
tictoc::tic()
pop_burst <- expand_grid(file = timeseries_files,
                         mtl = c("All", "H", "A", "E")) %>%
    mutate(burst = map2(file, mtl, function(ff, anat) {

        ## Reading data
        df <- readRDS(ff) %>%
            remove_lowneurons_traj() %>%
            remove_crosstalk_traj()

        if(no_probe) {
            ## Filtering out probe neurons
            probe_units <- type_unit %>%
                filter(probe) %>%
                filter(what == id_to_session(options_extract(ff)$ses)) %>%
                pull(unit_id)
            if(lt(probe_units) != 0) {
                cat("Removing ", lt(probe_units), "probe units\n")
                df <- select(df, -matches(probe_units))
            }
        }        
        if(anat != "All") {
            ## Filtering mtl region
            feat_list <- str_subset(str_subset(names(df), "feat"), paste0("_", anat, "[0-9]"))
            if(lt(feat_list) < 5) return(NULL)
            df <- select(df, what, trial, event, time, all_of(feat_list))
        }
        
        ## Calculating the thresholds for the burst event
        thr <- df %>%
            group_by(trial) %>%
            summarize(across(matches("feat_"), mean), .groups = "drop") %>%
            mutate(unit_mean = rowSums(across(matches("feat_"))),
                   .keep = "unused") %>% pull(unit_mean) %>%
            { mean(.) + 3*sd(.)  }
        ncell <- ncol(select(df, matches("feat_")))

        ## Finding burst events as time points over a certain threshols
        burst_events <- df %>%
            mutate(global = rowSums(across(matches("feat_"))),
                   .keep = "unused") %>%
            nest(data = -trial) %>%
            mutate(across(data, map, function(x) {
                mutate(x, burst_idx = seq_long(global > thr, TRUE, min_length = 10)) 
            })) %>%
            unnest(data) %>%
            mutate(burst_idx = ifelse(burst_idx == -1,
                                      0,
                                      paste0(str_extract(trial, "[0-9]+"), "_",
                                             str_pad(burst_idx, width = 2, side = "left", pad = 0)))) %>%
            filter(burst_idx != "0") %>% ## Removing the short ones
            group_by(burst_idx) %>%
            mutate(frate = mean(global)) %>%
            slice(c(1, n())) %>%
            mutate(boundaries = c("start", "end")) %>%
            ungroup() %>%
            select(-event, -global) %>%
            pivot_wider(names_from = "boundaries", values_from = "time") %>%
            mutate(duration = 0.01 + (end - start)) %>%
            add_column(ncell = ncell)

        ## Assigning an event (trial epoch) to each burst
        burst_events <- burst_events %>%
            mutate(across(c(start, end), list(event = ~ as.numeric(cut(start,
                                                                       breaks = event_breaks + 0.001,
                                                                       labels = event_levels,
                                                                       include.lowest = T))))) %>%
            mutate(event = ifelse(start_event == end_event, event_levels[start_event], "cross"),
                   .after = "what") %>%
            mutate(tmp = event_breaks[1 + start_event] - start) %>%
            mutate(event = case_when(event != "cross" ~ event,
                                     tmp > 0.8 * duration ~ event_levels[start_event],
                                     tmp < 0.2 * duration ~ event_levels[end_event],
                                     TRUE ~ "cross")) %>%
            ## filter(event != "cross") %>%
            select(-tmp, -start_event, -end_event) %>%
            left_join(distinct(filter(sel, what %in% unique(burst_events$what)), trial, what, response_time),
                      by = c("trial", "what")) %>% 
            mutate(event = case_when(event != "Probe" ~ event,
                                     start > response_time ~ "Probe_after",
                                     start + (end - start) * 0.8 <= response_time ~ "Probe",
                                     start + (end - start) * 0.2 > response_time ~ "Probe_after",
                                     TRUE ~ "Probe_cross"
                                     )) %>%
            select(-response_time)
        
        ## Cleaning of the dataset; i.e. removing the burst too close to each other
        ## Identifying the suspect pairs of burts (too close to each other)
        suspect <- burst_events %>%
            mutate(frate_prev = lag(frate), .after = "frate") %>%
            mutate(burst_idx_prev = lag(burst_idx), .after = "burst_idx") %>%
            mutate(dist = start - lag(end)) %>%
            filter(dist < 0.15 & trial == lag(trial))  %>%
            select(-what, -event, -ncell)
        if(nrow(suspect) != 0) {
            to_remove <- suspect %>%
                mutate(to_remove = ifelse(frate < frate_prev, burst_idx, burst_idx_prev)) %>%
                pull(to_remove) %>% unique()
            burst_events <- filter(burst_events, !burst_idx %in% to_remove)
        }
        
        return(burst_events)
    }))
tictoc::toc()


## Scheme of all the trials effectively involved; used for a correct normalization successively
## To be integrated in the above loop
tictoc::tic()
pop_burst_trials <- expand_grid(file = ls_file,
                               mtl = c("H", "A", "E")) %>%
    mutate(burst = map2(file, mtl, function(ff, anat) {
        cat(options_extract(basename(ff))$ses, "\n")
        df <- readRDS(ff) %>%
            remove_lowneurons_traj() %>%
            remove_crosstalk_traj()
        if(no_probe) {
            probe_units <- type_unit %>%
                filter(probe) %>%
                filter(what == id_to_session(options_extract(ff)$ses)) %>%
                pull(unit_id)
            if(lt(probe_units) != 0) {
                df <- select(df, -matches(probe_units))
            }
        }        
        feat_list <- str_subset(str_subset(names(df), "feat"), paste0("_", anat, "[0-9]"))
        if(lt(feat_list) < 5) return(NULL)
        distinct(df, what, trial)
    })) %>%
    select(-file) %>%
    unnest(burst) %>%
    mutate(across(mtl, factor, levels = c("H", "A", "E"),
                  labels = c("Hipp", "Amyg", "Ent"))) 
tictoc::toc()

## Accounting also for trials where bursts have not been identified
## By acting directly on the burst generation above this could be avoided (to be done) 
pop_burst <- pop_burst %>%
    filter(!map_lgl(burst, is.null)) %>%
    filter(mtl != "All") %>%
    select(-file) %>%
    unnest(burst) %>%
    mutate(across(mtl, factor, levels = c("H", "A", "E"),
                  labels = c("Hipp", "Amyg", "Ent"))) %>% 
    nest(data_burst = c(-mtl, -trial, -what)) %>%
    right_join(pop_burst_trials, by = c("mtl", "trial", "what")) %>% ## Adding trials ohne bursts
    left_join(distinct(sel, what, trial, set_size, response_time, correct),
              by = c("trial", "what")) ## Just adding trial infos for later

## Saving
saveRDS(pop_burst,
        here("data/processed", str_glue("Population_bursts{ifelse(no_probe, \"_noprobe\",\"\")}.RData")))
saveRDS(pop_burst_trials,
        here("data/processed", str_glue("Population_bursts_trialscheme{ifelse(no_probe, \"_noprobe\",\"\")}.RData")))



## ###########################
## Composition of the bursts; both in terms of smooth firing rates and number of spikes (binary)

## Reading population bursts
pop_burst <- readRDS(here("data/processed/Population_bursts.RData"))

## Reading timeseries of smooth firing rates
timeseries_files <- list.files(here("data/processed"),
                       pattern = "-mtlAll-ftsFRUnit1Hz-krnPyNorm25ms-bin10ms.RData",
                       full.names = T)

## Loop over the session
tictoc::tic()
burst_composition <- map_dfr(c.c(timeseries_files), function(ff) {
    cat(basename(ff), "\n")
    df <- c(smooth = ff,
            bin = str_remove(ff, "-krnPyNorm25ms")) %>%
        map_dfr(function(file) {
            ## Read the timeseries in another format
            readRDS(file) %>%
                remove_lowneurons_traj() %>%
                remove_crosstalk_traj() %>%
                pivot_longer(cols = starts_with("feat_"),
                             names_to = "unit_id", names_prefix = "feat_",
                             values_to = "frate") %>%
                mutate(mtl = factor(str_extract(unit_id, "[HAE]"), levels = c("H", "A", "E"),
                                    labels = c("Hipp", "Amyg", "Ent")))
        }, .id = "type_frate")
    bursts <- filter(pop_burst, what_id(what) == options_extract(ff)$ses)
    if(nrow(bursts) == 0) {
        return(NULL)
    }

    ## Computing for each burst the average firing rate on the single units.
    ## Implement a faster way. However this is quite clean and not terribly slow (~ 2 mins)
    composition <- bursts %>%
        unnest(data_burst) %>%
        nest_join(select(df, -what, -event), by = c("mtl", "trial"), name = "units") %>%
        mutate(units = pmap(list(units, start, end), function(unt, st, en) {
            ## For each burst I filter the dataset in the correct time window and compute the average firing rate of each unit
            unt %>%
                filter(between(time, st, en)) %>%
                group_by(type_frate, unit_id) %>%
                summarize(across(frate,
                                 ~ ifelse(unique(type_frate) == "bin", sum(.), mean(.))),
                          .groups = "drop") 
        })) 
    return(composition)
}, .id = "file")
tictoc::toc()

## Saving
saveRDS(burst_composition, here("data/processed", "Population_burst_unitcomposition.RData"))
