## Computing the LvR of combinations of units

library(here)
library(parallel)
library(tidyverse)

source(here("R/utils.R"))
source(here("R/utils_project.R"))
source(here("R/utils_bursty.R"))

ncores <- 3

event_levels <- c("Fixation", "Stimulus", "Maintenance", "Probe")
anatom_levels <- c("Hipp", "Amyg", "Ent")

## ####################
## Preamble: Reading data



main <- readRDS(here('data/processed/Allsessions_data.RData'))

sess_frate <- frate_selection(frate_threshold = 1) %>%
    select(what, ends_with("On")) %>%
    filter(rowSums(across(c(where(is.numeric), -Tot_On), ~ . >= 5)) > 0) # at least a brain region with minimum 5 neurons

sel <- main %>%
    filter(!artifact) %>%
    filter(what %in% sess_frate$what) %>%
    mutate_at("unit", map,
              ~ mutate(.x, unit_id = str_replace(unit_id, '_(?=[0-9]+$)', paste0('_', str_sub(mtl,1,1))))) %>%
    remove_lowneurons(frate_threshold = 1) %>%
    remove_crosstalk() %>%
    unnest(unit) %>%
    mutate_at("spike_time", map, ~ sort(.x)) %>%
    arrange(what, trial, unit_id)  %>%
    select(-tags, -artifact, -unit_number) 


## ###############################
## Production: computing the LvR for combinations of units in the different trial epochs (`event`)

cat("Starting creating the lvr dataset\n")
min_isi <- 3
k_max <- 4 # Maximum number of combination elements
event_breaks <- c(-6, -5, -3, 0)

## Computing first LvR for for single neurons
tictoc::tic()
lvr_df <- sel %>%
    select(-response, -probe_letter, -match) %>%
    mutate(across(c(mtl, set_size, correct), ~ fns_sel[[cur_column()]](.))) %>%
    mutate(spike_time = map2(spike_time, response_time, function(st, rp) {
        fct <- cut(st, breaks = c(event_breaks, rp), labels = event_levels, include.lowest = T)
        c(list(All = st), split(st, fct, drop = F))
    })) %>%
    unnest(spike_time) %>%
    mutate(event = names(spike_time), .after = 'unit_id') %>%
    mutate(across(spike_time, unname))  %>%
    mutate(lvr = map_dbl(spike_time, ~ lvr(.x, min_isi = min_isi, default = NA))) %>%
    nest(lvr_single = c(unit_id, spike_time, lvr))
tictoc::toc()
cat("Starting creating the combos\n")

## Computing then for combinations
tictoc::tic()
lvr_combos <- parallel::mclapply(X = lvr_df$lvr_single, FUN = function(df) {
    ## lvr_combos <- lapply(X = lvr_df$lvr_single, FUN = function(df) {
    df <- df %>%
        select(unit_id, spike_time) %>%
        deframe()
    if(lt(df) < 2) return(NULL)
    cat(lt(df), "\n")
    combos <- map(c.c(seq(2, min(c(lt(df), k_max)))), function(k) {
        ## cmb <- combn(df, k, simplify = F)
        ## idxx <- sort(sample(lt(cmb), min(c(max_ncombos, lt(cmb)))))
        combn(df, k, simplify = F) %>%
            {list(unit_id = map(., names),
                  lvr_composite = map_dbl(., function(x) {
                      lvr(unique(sort(unlist(x, use.names = F))), min_isi, default = NA, R = 0)
                  }))} %>%
            enframe() %>%
            pivot_wider() %>% unnest(col = everything()) %>%
            unnest_wider(unit_id, names_sep = ".") 
    }) %>%
        enframe(name = "combination", value = "results")
    return(combos)
},
mc.preschedule = T, #TRUE is twice as fast as FALSE (~45 vs ~90 secs)
mc.cores = ncores)
tictoc::toc()

## Saving all of the combos (up to 4)
lvr_df <- add_column(lvr_df, lvr_combos = lvr_combos)
saveRDS(lvr_df, here("data/processed", str_glue("LvR_single_combos4_minisi{min_isi}.RData")))
rm(lvr_combos)


## Restricting to max triplet for practical reasons (file too big)
lvr_df <- lvr_df %>%
    filter(!map_lgl(lvr_combos, is.null)) %>%
    mutate(across(lvr_combos, map, ~ filter(., as.numeric(combination) <= 3)))
aveRDS(lvr_df, here("data/processed", str_glue("LvR_single_combos3_minisi{min_isi}.RData")))

cat("Finished!")


## Saving again in a better and more compact format 
cat("REading the Triples\n")
tictoc::tic()
triple <- readRDS(here("data/processed", str_glue("LvR_single_combos3_minisi{min_isi}.RData"))) %>%
    mutate(lvr_combos = map2(lvr_single, lvr_combos, function(sg, cb) {
        sg <- select(sg, unit_id, lvr_composite = lvr) %>%
            mutate(combination = "1") %>%
            nest(results= -combination)
        ifelse.c(!is.null(cb), bind_rows(sg, cb), sg)
    })) %>%
    select(-lvr_single) %>%
    unnest(lvr_combos) %>%
    mutate(lvr = map(results, "lvr_composite"))
tictoc::toc()
saveRDS(triple, here("data/processed", str_glue("LvR_single_combos3_minisi{min_isi}_betterformat.RData")))


## ###########
## Production: Same as above but computing within sliding windows of 2sec with 0.25 step

k_max <- 4 # Maximum number of combination elements
min_isi <- 3

## Reading the windowed values
cat("Reading the sliding window weights\n")
lvrwind_df <- readRDS(here("data/processed",
                           str_glue('LvR_FRate_Cv_weights_bin50ms_win2s.RData'))) %>%
    mutate(mtl = str_extract(unit_id, "[AHE]"), .after = "trial") %>%
    mutate(mtl = factor(mtl, levels = c("H", "A", "E"), labels = anatom_levels)) %>%
    mutate_at("metric", map, function(df) {
        ## This simpler solution is way faster than `mutate` one (15x)
        x <- replace(df$lvr, df$lvr < 0, NA)
        df$lvr <- x
        x <- replace(df$cv, df$cv < 0, NA)
        df$cv <- x
        return(df)
    }) %>%
    unnest(metric) %>%
    select(-spike_time) %>%
    mutate(across(c(time, start, end), round, digit = 4)) %>%
    filter(time %in% c(seq(-4, 2, by = 0.25), 1.95))  %>%
    arrange(time) %>%
    mutate(wind_id = as.numeric(fct_inorder(as.chr(time)))) %>%
    mutate(wind_id = str_pad(wind_id, width = 2, side = 'left', pad = 0)) %>%
    mutate(event = as.chr(str_glue("wind{wind_id}_{time}")), .after = "mtl", .keep = "unused") %>%
    select(-start, -end) %>%
    rename(spike_time = "spk") %>%
    select(-frate, -cv) %>%
    nest(lvr_single = c(unit_id, spike_time, lvr)) %>%
    left_join(distinct(sel, what, trial, set_size, response_time, correct), by = c("what", "trial")) %>%
    mutate(across(c(set_size, correct), ~ fns_sel[[cur_column()]](.))) %>% 
    relocate("lvr_single", .after = "response_time") %>%
    arrange(what, trial, mtl, event)

## Computing the LvR combinations
tictoc::tic()
lvr_combos <- parallel::mclapply(X = lvrwind_df$lvr_single, FUN = function(df) {
    df <- df %>%
        select(unit_id, spike_time) %>%
        deframe()
    if(lt(df) < 2) return(NULL)
    cat(lt(df), "\n")
    combos <- map(c.c(seq(2, min(c(lt(df), k_max)))), function(k) {
        combn(df, k, simplify = F) %>%
            {list(unit_id = map(., names),
                  lvr_composite = map_dbl(., function(x) {
                      lvr(unique(sort(unlist(x, use.names = F))), min_isi, default = NA, R = 0)
                  }))} %>%
            enframe() %>%
            pivot_wider() %>% unnest(col = everything()) %>%
            unnest_wider(unit_id, names_sep = ".") 
    }) %>%
        enframe(name = "combination", value = "results")
    return(combos)
},
mc.preschedule = T, #TRUE is twice as fast as FALSE (~45 vs ~90 secs)
mc.cores = ncores)
tictoc::toc()
lvrwind_df <- lvrwind_df %>%
    add_column(lvr_combos = lvr_combos)
rm(lvr_combos)
saveRDS(lvrwind_df, here("data/processed", str_glue("LvR_single_combos4_minisi{min_isi}_window2s.RData")))

## Restricting to max triplet for practical reasons (file too big)
lvrwind_df <- lvrwind_df %>%
    filter(!map_lgl(lvr_combos, is.null)) %>%
    mutate(across(lvr_combos, map, ~ filter(., as.numeric(combination) <= 3)))
saveRDS(lvrwind_df, here("data/processed", str_glue("LvR_single_combos3_minisi{min_isi}_window2s.RData")))

## Saving again in a better and more compact format 
tictoc::tic()
triple_wind <- readRDS(here("data/processed", str_glue("LvR_single_combos3_minisi{min_isi}_window2s.RData"))) %>%
    mutate(lvr_combos = map2(lvr_single, lvr_combos, function(sg, cb) {
        sg <- select(sg, unit_id, lvr_composite = lvr) %>% mutate(combination = "1") %>% nest(results= -combination)
        ifelse.c(!is.null(cb), bind_rows(sg, cb), sg)
    })) %>%
    select(-lvr_single) %>%
    unnest(lvr_combos) %>%
    mutate(lvr = map(results, "lvr_composite"))
tictoc::toc()
saveRDS(triple_wind, here("data/processed", str_glue("LvR_single_combos3_minisi{min_isi}_window2s_betterformat.RData")))



## ####################
## Other windowed quantities

min_isi <- 3
window_s <- 2

single_df <- readRDS(here("data/processed", str_glue("LvR_single_combos3_minisi{min_isi}_window2s_betterformat.RData"))) %>%
    filter(combination == '1') %>%
    select(-correct, -set_size, -combination) %>%
    select(-lvr) %>%
    unnest(results) %>%
    rename('lvr' = 'lvr_composite', 'time' = 'event')

time_levels <- sort(unique(single_df$time))
sel_times <- str_extract(time_levels, "(?<=_).*")

windowed_df <- readRDS(here("data/processed", str_glue('LvR_FRate_Cv_weights_bin50ms_win{window_s}s.RData'))) %>%
    select(-spike_time) %>%
    unnest(metric) %>%
    select(what, trial, unit_id, time, frate) %>%
    filter(time >= -4)  %>%
    filter(round(time, 3) %in% as.numeric(sel_times)) %>%
    mutate(time = factor(as.chr(time), levels = sel_times, labels = time_levels)) 

single_df <- single_df %>%
    left_join(windowed_df, by = c("what", "trial", "unit_id", "time"))

saveRDS(single_df, here("data/processed", str_glue("LvR_single_Nocombos_minisi{min_isi}_window2s_betterformat.RData")))

