library(here)
library(tidyverse)
library(cowplot)

source(here("R/utils.R"))
source(here("R/utils_project.R"))
source(here("R/utils_bursty.R"))

## ##############
## Figure 1a

brk <- c(-6, -5, -3, 0, 2)
mid <- brk[-5] + diff(brk) / 2
label <- tibble(labs = c("Fixation\n1 s", " Encoding\n 2 s", " Maintenance\n 3 s", "\nRetrieval"), x = mid)

create_vertices <- function(center, side) {
    hs <- side / 2
    tibble(x = c(center[1] - hs, center[1] + hs, center[1] + hs, center[1] - hs),
           y = c(center[2] + hs, center[2] + hs, center[2] - hs, center[2] - hs)) %>%
        { add_row(., .[1,]) }
}

ycent <- -0.8
squares <- map_dfr(c.c(mid), ~ create_vertices(c(.x, ycent), 1.1), .id = "grp") %>%
    mutate(alpha = as.numeric(plyr::mapvalues(grp, from = unique(grp), to = c(0.4, 1, 0.4, 1))))

## https://www.historyhit.com/key-battles-in-the-napoleonic-wars/
pl_experiment <- tibble(x = brk) %>%
    slice(-c(1, n())) %>%
    ggplot() +
    geom_point(aes(x, y = 0), size = 5,
               shape = 21, color = "#67000d", fill = alpha("#67000d", 0.5),
               stroke = 3) +
    geom_text(data = label, aes(x =x, label = labs, y = 0.6), size = 6,
              vjust = c(rep(0.5, 3), 0.25)) +
    geom_path(data = squares,
              aes(x = x, y = y, group = grp), color = "black", size = 0.8,
              alpha = squares$alpha) +
    geom_segment(x = -6, xend = 2, y = 0, yend = 0, lineend = "butt", color = "#67000d",
                 size = 1) +
    annotate("point", x = brk[c(1)], y = 0, size = 5.3,
             shape = 21, color = "#08306b", fill = alpha("#08306b", 0.5),
             stroke = 3) +
    annotate("point", x = brk[c(5)], y = 0, size = 5.3,
             shape = 4, color = "#08306b", fill = alpha("#08306b", 0.5),
             stroke = 3) +
    annotate("point", x = mid[1], y = ycent, shape = 22, size = 2.7, 
             alpha = 0.5, stroke = 1, fill = "black") +
    annotate("point", x = mid[3], y = ycent, shape = 22, size = 2.7,
             alpha = 0.5, stroke = 1, fill = "black") +
    annotate("text", x = mid[2], y = ycent, label = "XPMT·JBLX", size = 3.2,
             family = "sans") +
    annotate("text", x = mid[4], y = ycent, label = "W", size = 4.5) +
    annotate("text", x = 2, y = 1.05, label = "Response\nIN/OUT", size = 5.8, vjust = 0) +
    annotate("segment",
             xend = 2, x = 2,
             y = 0.9, yend = 0.3,
             arrow = arrow(length = unit(0.05, "npc"))) +
    theme_nothing(font_size = 18) +
    theme(plot.margin = margin(r = 0.5, unit = "cm")) +
    coord_fixed(ylim = c(NA, 0.9), xlim = c(-6, 1.7), clip = "off")

saveRDS(pl_experiment, here("plots/RData", "Experiment_description.RData"))


## ##############
## Figure 1b

main <- readRDS(here('data/processed/Allsessions_data.RData'))

sess_frate <- frate_selection(frate_threshold = 1) %>%
    select(what, ends_with("On")) %>%
    filter(rowSums(across(c(where(is.numeric), -Tot_On), ~ . >= 5)) > 0) # at least a brain region with minimum 5 neurons

sel <- main %>%
    filter(!artifact) %>%
    filter(what %in% sess_frate$what) %>%
    mutate_at("unit", map,
              ~ mutate(.x, unit_id = str_replace(unit_id, '_(?=[0-9]+$)', paste0('_', str_sub(mtl,1,1))))) %>%
    remove_lowneurons(frate_threshold = 1) %>%
    remove_crosstalk() %>%
    unnest(unit) %>%
    mutate_at("spike_time", map, ~ sort(.x)) %>%
    arrange(what, trial, unit_id) %>%
    select(-tags, -artifact, -unit_number, -response, -probe_letter) %>%
    mutate(across(c(set_size, correct, match), ~ fns_sel[[cur_column()]](.)))

window8 <- sel %>%
    mutate(across(spike_time, unname)) %>%
    mutate(lvr = map_dbl(spike_time, lvr, default = -1)) %>%
    mutate(frate = map_dbl(spike_time, ~ lt(.x) / 8)) %>%
    filter(lvr > 0)

firing_labeller <- function(var) {
    nm <- c("Regular" = "Regular\nLvR∼0.5",
            "Poisson"= "Poisson\n∼1",
            "Bursty"= "Bursty\n∼1.5")
    return(nm[var])
}
 
set.seed(144)
fct_lvr <- c("Regular", "Poisson", "Bursty")
df_burst_example <- window8 %>%
    filter(frate > 2) %>%
    mutate(lvr_type = case_when(between(lvr, 0.45, 0.55) ~ fct_lvr[1],
                                between(lvr, 0.95, 1.05) ~ fct_lvr[2],
                                between(lvr, 1.45, 1.55) ~ fct_lvr[3],
                                T ~ "NA")) %>%
    filter(lvr_type != "NA") %>%
    mutate_at("lvr_type", factor, levels = rev(fct_lvr)) %>%
    group_by(lvr_type) %>%
    sample_n(size = 3) %>%
    mutate(id = seq(n())) %>%
    ungroup()
cp_example <- readRDS(here("data/processed/ChangePoint_binomial_adaptive.RData")) %>%
    select(what, trial, unit_id, cp) %>%
    right_join(df_burst_example, by = c("what", "trial", "unit_id")) %>%
    unnest(cp) %>%
    filter(truepoint) %>%
    select(-truepoint, -match, -correct, -response_time, -spike_time, -lvr, -frate)

pl_burst_example <- df_burst_example %>%
    unnest(spike_time) %>%
    ggplot(aes(spike_time, id)) +
    geom_point(aes(color = lvr_type), shape = 124, size = 8) +
    geom_vline(xintercept = c(-6, -5, -3, 0, 2), alpha = 0.4, linetype = 2) +
    geom_point(data = cp_example, aes(x = cp_time, y = id)) +
    scale_color_manual(values = c(setNames(Redmonder::redmonder.pal(11, "dPBIYlBu")[c(3,9)], c("Bursty", "Regular")),
                                  "Poisson" = "grey70")) +
    facet_grid(lvr_type ~ ., switch = "y",
               labeller = labeller(lvr_type = firing_labeller)) +
    theme_cowplot(font_size = 20) +
    scale_x_continuous("Time from probe [s]", expand = expansion(mult = c(0.03,0.03))) +
    scale_y_continuous(expand = expansion(mult = c(0.22, 0.22))) +
    coord_cartesian(clip = "off") +
    theme(legend.position = "none",
          plot.margin = margin(t = 1, b = 0.3, r = 0.3, unit = 'cm'),
          axis.text.y = element_blank(),
          axis.ticks.y = element_blank(),
          axis.title.y = element_blank(),
          strip.placement = "outside",
          strip.text = element_text(size = 20),
          strip.background = element_rect(fill = "white"))

saveRDS(pl_burst_example, here("plots/RData/Example_burstiness.RData"))


